# Cordova Device apps plugin

Plugin to get the list of installed applications on Android.

## Installation

run:
`cordova plugin add cordova-plugin-device-apps`

## API

The API is available as a global `DeviceApps` object

### Get list of installed applications

```javascript
DeviceApps.getInstalledApplications(function successCallback, [function errorCallback], [options]);
```

Params:

- `successCallback`: A function that takes a single argument. The argument will be an array of objects. See example later for what this looks like
- `errorCallback` (not required): A function that takes a single argument. The argument will be an error message.
- `options` (not required): An object. The object can have three properties. By default the options are false.
    - `systemApps`: If true, system apps will be included
    - `includeAppIcons`: If true, a base64-encoded app icon will be included for each app
    - `onlyAppsWithLaunchIntent`: If true, only apps that can be launched will be included
    
    
### Get information about a single app

```javascript
DeviceApps.getApp(function successCallback, [function errorCallback], options);
```

Params:

- `successCallback`: A function that takes a single argument. The argument will be a single object. See example later for what this looks like
- `errorCallback` (not required): A function that takes a single argument. The argument will be an error message.
- `options`: An object. The `appName` field is required. The other is not.
    - `appName`: If true, system apps will be included
    - `includeAppIcon`: If true, a base64-encoded app icon will be included for the app
    

### Check if app is installed

```javascript
DeviceApps.isInstalled(function successCallback, [function errorCallback], name);
```

Params:

- `successCallback`: A function that takes a single argument. The argument will be true if the app is installed or false otherwise.
- `errorCallback` (not required): A function that takes a single argument. The argument will be an error message.
- `name`: The `packageName` of the app to check


### Open an app

```javascript
DeviceApps.openApp(function successCallback, [function errorCallback], name);
```

Params:

- `successCallback`: A function that takes a single argument. The argument will be true if the app is opened or false otherwise.
- `errorCallback` (not required): A function that takes a single argument. The argument will be an error message.
- `name`: The `packageName` of the app to open


### Application object format

The format of the application object returned in `getInstalledApplications` and `getApp` is as follows. The `appIcon` will only be present if requested in the call.

```json
{
  "appName": "Friendly name (package label)",
  "packageName": "Package name (like com.google.awesome)",
  "versionCode": 1010,
  "versionName": "Version name (like 1.0.1)",
  "dataDir": "Application's data directory",
  "systemApp": true,
  "appIcon": "base64-encoded image"
}
```

### Credits

The bulk of this plugin is copied from [this Flutter plugin](https://github.com/g123k/flutter_plugin_device_apps) by [@g123k](https://github.com/g123k).

Thanks!
