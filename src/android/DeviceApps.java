package com.fallingdeathgames;

import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.drawable.Drawable;
import android.util.Base64;
import org.apache.cordova.CallbackContext;
import org.apache.cordova.CordovaPlugin;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONException;

import android.app.Activity;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;

import java.io.ByteArrayOutputStream;
import java.util.List;

public class DeviceApps extends CordovaPlugin {
    private final int SYSTEM_APP_MASK = ApplicationInfo.FLAG_SYSTEM | ApplicationInfo.FLAG_UPDATED_SYSTEM_APP;

    @Override
    public boolean execute(String action, JSONArray args, final CallbackContext callbackContext) {
        JSONObject options = args.optJSONObject(0);
        String packageName;

        switch(action) {
            case "getInstalledApps":
                boolean systemApps = options != null && options.optBoolean("systemApps", false);
                boolean includeAppIcons = options != null && options.optBoolean("includeAppIcons", false);
                boolean onlyAppsWithLaunchIntent = options != null && options.optBoolean("onlyAppsWithLaunchIntent", false);
                fetchInstalledApps(systemApps, includeAppIcons, onlyAppsWithLaunchIntent, callbackContext);
                break;

            case "getApp":
                packageName = options != null ? options.optString("appName", null) : null;
                if (packageName == null) {
                    callbackContext.error("Missing package name");
                } else {
                    boolean includeAppIcon = options.optBoolean("includeAppIcon", false);
                    try {
                        JSONObject result = getApp(packageName, includeAppIcon);
                        if (result == null) {
                            callbackContext.error("App not found");
                        } else {
                            callbackContext.success(result);
                        }
                    } catch (JSONException ex) {
                        callbackContext.error("Error getting application data");
                    }
                }
                break;
            case "isInstalled":
                args.isNull(0);
                packageName = args.optString(0, null);
                if (packageName == null || args.isNull(0)) {
                    callbackContext.error("Missing package name");
                } else {
                    callbackContext.success(isAppInstalled(packageName) ? 1 : 0);
                }
                break;
            case "openApp":
                packageName = args.optString(0, null);
                if (packageName == null || args.isNull(0)) {
                    callbackContext.error("Missing package name");
                } else {
                    callbackContext.success(openApp(packageName) ? 1 : 0);
                }
                break;
            default:
                return false;
        }
        return true;
    }

    private void fetchInstalledApps(final boolean includeSystemApps, final boolean includeAppIcons, final boolean onlyAppsWithLaunchIntent, final CallbackContext callbackContext) {
        cordova.getThreadPool().execute(() -> callbackContext.success(getInstalledApps(includeSystemApps, includeAppIcons, onlyAppsWithLaunchIntent)));
    }

    private JSONArray getInstalledApps(boolean includeSystemApps, boolean includeAppIcons, boolean onlyAppsWithLaunchIntent) {
        Context context = getContext();
        PackageManager packageManager = context.getPackageManager();
        List<PackageInfo> apps = packageManager.getInstalledPackages(0);
        JSONArray installedApps = new JSONArray();

        for (PackageInfo pInfo : apps) {
            if (!includeSystemApps && isSystemApp(pInfo)) {
                continue;
            }
            if (onlyAppsWithLaunchIntent && packageManager.getLaunchIntentForPackage(pInfo.packageName) == null) {
                continue;
            }

            try {
                JSONObject map = getAppData(packageManager, pInfo, includeAppIcons);
                installedApps.put(map);
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }

        return installedApps;
    }

    private boolean isSystemApp(PackageInfo pInfo) {
        return (pInfo.applicationInfo.flags & SYSTEM_APP_MASK) != 0;
    }

    private JSONObject getAppData(PackageManager packageManager, PackageInfo pInfo, boolean includeAppIcon) throws JSONException {
        JSONObject map = new JSONObject();
        map.put("appName", pInfo.applicationInfo.loadLabel(packageManager).toString());
        map.put("packageName", pInfo.packageName);
        map.put("versionCode", pInfo.versionCode);
        map.put("versionName", pInfo.versionName);
        map.put("dataDir", pInfo.applicationInfo.dataDir);
        map.put("systemApp", isSystemApp(pInfo));

        if (includeAppIcon) {
            try {
                Drawable icon = packageManager.getApplicationIcon(pInfo.packageName);
                String encodedImage = encodeToBase64(getBitmapFromDrawable(icon));
                map.put("appIcon", encodedImage);
            } catch (PackageManager.NameNotFoundException ignored) {
            }
        }

        return map;
    }

    private String encodeToBase64(Bitmap image) {
        ByteArrayOutputStream byteArrayOS = new ByteArrayOutputStream();
        image.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOS);
        return Base64.encodeToString(byteArrayOS.toByteArray(), Base64.NO_WRAP);
    }

    private Bitmap getBitmapFromDrawable(Drawable drawable) {
        final Bitmap bmp = Bitmap.createBitmap(drawable.getIntrinsicWidth(), drawable.getIntrinsicHeight(), Bitmap.Config.ARGB_8888);
        final Canvas canvas = new Canvas(bmp);
        drawable.setBounds(0, 0, canvas.getWidth(), canvas.getHeight());
        drawable.draw(canvas);
        return bmp;
    }

    private Activity getAppActivity() {
        return cordova.getActivity();
    }

    private Context getContext() {
        return getAppActivity().getApplicationContext();
    }

    private JSONObject getApp(String packageName, boolean includeAppIcon) throws JSONException {
        try {
            PackageManager packageManager = getContext().getPackageManager();
            return getAppData(packageManager, packageManager.getPackageInfo(packageName, 0), includeAppIcon);
        } catch (PackageManager.NameNotFoundException ignored) {
            return null;
        }
    }

    private boolean isAppInstalled(String packageName) {
        try {
            getContext().getPackageManager().getPackageInfo(packageName, 0);
            return true;
        } catch (PackageManager.NameNotFoundException ignored) {
            return false;
        }
    }

    private boolean openApp(String packageName) {
        Context context = getContext();
        Intent launchIntent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        if (launchIntent != null) {
            // null pointer check in case package name was not found
            context.startActivity(launchIntent);
            return true;
        }
        return false;
    }
}
