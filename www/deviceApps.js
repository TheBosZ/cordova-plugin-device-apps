var exec = require('cordova/exec');

exports.getInstalledApplications = function(resultCallback, errorCallback, options) {
	if (typeof options === 'undefined' && typeof errorCallback !== 'function') {
		options = errorCallback;
		errorCallback = function(param){};
	}
	exec(resultCallback, errorCallback, "DeviceApps", "getInstalledApps", [options]);
};

exports.getApp = function(resultCallback, errorCallback, options) {
	if (typeof options === 'undefined' && typeof errorCallback !== 'function') {
		options = errorCallback;
		errorCallback = function(param){};
	}
	exec(resultCallback, errorCallback, "DeviceApps", "getApp", [options]);
};

exports.isAppInstalled = function(resultCallback, errorCallback, name) {
	if (typeof name === 'undefined' && typeof errorCallback !== 'function') {
		options = errorCallback;
		errorCallback = function(param){};
	}
	exec(function(result){
		resultCallback(result === 1);
	}, errorCallback, "DeviceApps", "isAppInstalled", [name]);
};

exports.openApp = function(resultCallback, errorCallback, name) {
	if (typeof name === 'undefined' && typeof errorCallback !== 'function') {
		options = errorCallback;
		errorCallback = function(param){};
	}
	exec(function(result){
		resultCallback(result === 1);
	}, errorCallback, "DeviceApps", "openApp", [name]);
};
